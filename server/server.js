const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

app.get('/', function (req, res) {
  res.json({
    cats: 'http://localhost:3000/cats',
  });
});
app.get('/cats', function (req, res) {
  res.json([
    {
      type: 'cat',
      name: 'Vasia',
      status: 'Adopted',
      dateAdopted: '12/24/2016',
      adoptedBy: 'Marie Cypress',
      birthday: '10/01/2016',
      description: `Hullo hullo! I'm Vasia, and I have very strong views on the proper habitat for my gorgeousness, which involve hovering servants (you), a warm spot to rest on (your lap), and luxurious accommodations (your bed). Since despite my many wonderful qualities I lack opposable thumbs, you'll have to feed me when I meow.`,
      imageUrl: 'https://i.imgur.com/0ujbIy7.png',
    },
    {
      type: 'cat',
      name: 'Chowder',
      status: 'In Shelter',
      dateAdopted: null,
      adoptedBy: null,
      birthday: '08/30/2020',
      description: `Meet Chowder -- a super, nurturing kitty! She just came in with her brother leaving Chowder wondering where her forever home is. Her favorites hobbies include eating and being fed. Give us a call for more info!`,
      imageUrl: 'https://i.imgur.com/d1uRR9q.png',
    },
  ]);
});
app.get('/dogs', function (req, res) {
  res.json([
    {
      type: 'dog',
      name: 'Miyagi',
      status: 'Adopted',
      dateAdopted: '02/12/2021',
      adoptedBy: 'Jack & Emily Martin',
      birthday: '01/01/2018',
      description:
        'Hi! My name is Miyagi and I just arrived at the shelter. I’m a little nervous around new people but I’m friendly once I get to know you. I’ll absolutely adore you if you give me belly rubs! Looking for my furever home preferably adult only or adolescents!',
      imageUrl: 'https://i.imgur.com/trdRgiD.png',
    },
    {
      type: 'dog',
      name: 'Gazpacho',
      status: 'In Shelter',
      dateAdopted: null,
      adoptedBy: null,
      birthday: '04/25/2020',
      description: `Hi my name is Mike the Microphone. I was dumped at the shelter in a carrier. My sight isn't so well, but I love people even though i cant see them to well. I'm hoping I can enjoy my years with a family who can live me and that I can love.`,
      imageUrl: 'https://i.imgur.com/P2h46D4.png',
    },
    {
      type: 'dog',
      name: 'Lyre',
      status: 'In Shelter',
      dateAdopted: null,
      adoptedBy: null,
      birthday: '08/01/2009',
      description: `Hi I'm Lyre!!! I am 12 years old and looking for a home that will give me love until the end of time! I'm not too demanding of your attention but do like cuddles and kisses. Give me love and I'll surely return the favor. I am very quiet, take several naps during the day, I love to lay next to you as you work! Just knowing you are there makes me happy! I do like my daily walks and I LOVE going on car rides!!! I'll happily be in the back seat looking out the window! You even know I'm there! So what do you say? Want to be forever roomies??? I'm all yours!`,
      imageUrl: 'https://i.imgur.com/YQaUsjs.png',
    },
    {
      type: 'dog',
      name: 'Terra',
      status: 'Adopted',
      dateAdopted: '07/25/2021',
      adoptedBy: 'Ryan & Emma Wachowski',
      birthday: '02/11/2019',
      description: `Hi my name is Terra (short for Terra-fic!) and I recently arrived to the shelter. I am a sweet dog who loves rubs and attention. I don’t like enclosed spaces so I’m not doing too well in my kennel but I can’t wait to get adopted and be able to walk all around my new home!`,
      imageUrl: 'https://i.imgur.com/1kv994K.png',
    },
  ]);
});
app.post('/cats', function (req, res) {
  // we would call a db adapter and save the data to the database here

  res.sendStatus(200);
});
app.post('/dogs', function (req, res) {
  // we would call a db adapter and save the data to the database here

  res.sendStatus(200);
});

app.listen(3000);
