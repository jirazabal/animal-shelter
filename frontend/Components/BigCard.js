import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

export function BigCard({ image, title, description, emoji }) {
  return (
    <Card style={{ maxWidth: 1200, maxHeight: 800 }}>
      <CardMedia sx={{ height: 140 }} title={title} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {title}
        </Typography>
        <img
          style={{ maxWidth: 800, height: 500, borderRadius: '5px' }}
          src={image}
        />
        <Typography variant="body2">{description}</Typography>
      </CardContent>
      <CardActions>
        <Button size="small">{emoji}</Button>
      </CardActions>
    </Card>
  );
}
