import DateFnsUtils from '@date-io/date-fns';
import { Box, Button, Grid, Modal, TextField } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import Collapse from '@material-ui/core/Collapse';
import { red } from '@material-ui/core/colors';
import IconButton from '@material-ui/core/IconButton';
import { styled } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import axios from 'axios';
import React, { useEffect } from 'react';
import FlexView from 'react-flexview/lib';

const modalStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  width: '25%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  borderRadius: 5,
  boxShadow: 24,
  p: 4,
};
const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export function SmallCard({
  propName,
  propType,
  propDescription,
  propDateAdopted,
  propAdoptedBy,
  propStatus,
  propBirthday,
  imageUrl,
}) {
  const [selectedDOBDate, setSelectedDOBDate] = React.useState(
    propBirthday ? new Date(propBirthday) : null
  );
  const [selectedAdoptedDate, setSelectedAdoptedDate] = React.useState(
    propDateAdopted ? new Date(propDateAdopted) : null
  );
  const [name, setName] = React.useState(propName);
  const [type, setType] = React.useState(propType);
  const [description, setDescription] = React.useState(propDescription);
  const [adoptedBy, setAdoptedBy] = React.useState(propAdoptedBy);
  const [status, setStatus] = React.useState(propStatus);
  const [expanded, setExpanded] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    setName(name);
    setDescription(description);
    setSelectedAdoptedDate(selectedAdoptedDate);
    setAdoptedBy(adoptedBy);
    setStatus(status);
    setSelectedDOBDate(selectedDOBDate);
    setDescription(description);
  }, [
    name,
    description,
    selectedAdoptedDate,
    adoptedBy,
    status,
    selectedDOBDate,
    description,
  ]);

  const handleDOBDateChange = (date) => {
    setSelectedDOBDate(date);
  };
  const handleAdoptedDateChange = (date) => {
    setSelectedAdoptedDate(date);
  };
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    await axios.post(
      `http://localhost:3000/${propType}s`,
      {
        name,
        description,
        selectedAdoptedDate,
        adoptedBy,
        status,
        selectedDOBDate,
        description,
      },
      { headers: { 'Access-Control-Allow-Origin': '*' } }
    );
    handleClose();
  };

  return (
    <Card style={{ width: '400px', marginTop: '25px' }} sx={{ maxWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="avatar">
            {name[0]?.toUpperCase() ?? ''}
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={name}
        subheader={
          'Date of Birth: ' + new Intl.DateTimeFormat().format(selectedDOBDate)
        }
      />
      <CardMedia
        sx={{
          height: 0,
          paddingTop: '56.25%', // 16:9
        }}
        title="card"
      />
      <img
        src={imageUrl}
        style={{
          width: '400px',
          height: '245px',
        }}></img>
      <CardContent>
        <Typography variant="body2">{description}</Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton onClick={handleOpen} aria-label="edit">
          <EditIcon />
        </IconButton>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description">
          <Box sx={modalStyle}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
              <FlexView>
                <FlexView>{name}</FlexView>
                <FlexView marginLeft="auto">
                  <Avatar alt={name} src={imageUrl} />
                </FlexView>
              </FlexView>
            </Typography>
            <Typography
              component={'span'}
              id="modal-modal-description"
              sx={{ mt: 2 }}>
              <Grid>
                <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                  <Grid item>
                    <FlexView>
                      <div
                        style={{
                          transform: 'translate(0%,20%)',
                          width: '120px',
                          paddingBottom: '15px',
                        }}>
                        Name
                      </div>
                      <TextField
                        onInput={(e) => setName(e.target.value)}
                        id="name"
                        style={{ width: '50%' }}
                        label=""
                        placeholder={name}
                      />
                    </FlexView>
                  </Grid>

                  <Grid item>
                    <FlexView>
                      <div
                        style={{
                          transform: 'translate(0%,20%)',
                          width: '120px',
                          paddingBottom: '15px',
                        }}>
                        Description
                      </div>
                      <TextField
                        id="desc"
                        style={{ width: '50%' }}
                        onInput={(e) => setDescription(e.target.value)}
                        label=""
                        multiline
                        placeholder={description}
                      />
                    </FlexView>
                  </Grid>

                  <Grid item>
                    <FlexView>
                      <div
                        style={{
                          transform: 'translate(0%,20%)',
                          width: '120px',
                          paddingBottom: '15px',
                        }}>
                        Date of Birth
                      </div>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          onInput={handleDOBDateChange}
                          id="dob"
                          autoOk
                          variant="inline"
                          label=""
                          format="MM/dd/yyyy"
                          value={selectedDOBDate}
                          onChange={handleDOBDateChange}
                        />
                      </MuiPickersUtilsProvider>
                    </FlexView>
                  </Grid>

                  <Grid item>
                    <FlexView>
                      <div
                        style={{
                          transform: 'translate(0%,20%)',
                          width: '120px',
                          paddingBottom: '15px',
                        }}>
                        Adopted By
                      </div>
                      <TextField
                        onInput={(e) => setAdoptedBy(e.target.value)}
                        id="adoptedBy"
                        label=""
                        placeholder={adoptedBy}
                      />
                    </FlexView>
                  </Grid>

                  <Grid item>
                    <FlexView>
                      <div
                        style={{
                          transform: 'translate(0%,20%)',
                          width: '120px',
                          paddingBottom: '15px',
                        }}>
                        Status
                      </div>
                      <TextField
                        onInput={(e) => setStatus(e.target.value)}
                        id="status"
                        label=""
                        placeholder={status}
                      />
                    </FlexView>
                  </Grid>

                  <Grid item>
                    <FlexView>
                      <div
                        style={{
                          transform: 'translate(0%,20%)',
                          width: '120px',
                          paddingBottom: '15px',
                        }}>
                        Adopted Date
                      </div>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          onInput={handleAdoptedDateChange}
                          id="adoptedDate"
                          autoOk
                          variant="inline"
                          label=""
                          format="MM/dd/yyyy"
                          value={selectedAdoptedDate}
                          onChange={handleAdoptedDateChange}
                        />
                      </MuiPickersUtilsProvider>
                    </FlexView>
                  </Grid>
                  <Grid item>
                    <FlexView>
                      <div
                        style={{
                          transform: 'translate(0%,20%)',
                          width: '120px',
                          paddingBottom: '15px',
                        }}>
                        Photo
                      </div>
                      <Button
                        onClick={() => alert('TODO - nice feature to have!')}
                        variant="contained"
                        color="default">
                        Upload
                      </Button>
                    </FlexView>
                  </Grid>
                  <Grid item>
                    <FlexView>
                      <FlexView>
                        <div></div>
                      </FlexView>
                      <FlexView marginLeft="auto">
                        <Button
                          type="submit"
                          variant="contained"
                          color="primary">
                          Submit
                        </Button>
                      </FlexView>
                    </FlexView>
                  </Grid>
                </form>
              </Grid>
            </Typography>
          </Box>
        </Modal>
        {status.toUpperCase() === 'ADOPTED' && (
          <ExpandMore
            expand={expanded}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more">
            <ExpandMoreIcon />
          </ExpandMore>
        )}
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>
            {adoptedBy ? 'Adopted By: ' + adoptedBy : ''}
          </Typography>
          <Typography paragraph>
            {selectedAdoptedDate
              ? 'Date Adopted:' +
                new Intl.DateTimeFormat().format(selectedAdoptedDate)
              : ''}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
