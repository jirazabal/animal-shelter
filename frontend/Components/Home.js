import React from 'react';
import FlexView from 'react-flexview/lib';
import { BigCard } from './BigCard';
import kitten from '../static/images/cards/kitten.jpg';
import dog from '../static/images/cards/shiba.jpg';
import { Link } from 'react-router-dom';

export function Home() {
  return (
    <FlexView className="flex-container" hAlignContent="center">
      <FlexView style={{ paddingRight: '200px' }}>
        <Link
          to={`/cats`}
          style={{
            paddingLeft: '15px',
            textDecoration: 'none',
            color: 'inherit',
          }}>
          <BigCard
            image={kitten}
            title="Cats"
            description="Our adorable purr-fect furry family members who are looking for their forever home."
            emoji="🐈‍⬛"></BigCard>
        </Link>
      </FlexView>
      <FlexView>
        <Link
          to={`/dogs`}
          style={{
            paddingLeft: '15px',
            textDecoration: 'none',
            color: 'inherit',
          }}>
          <BigCard
            image={dog}
            title="Dogs"
            description="Humankind's best friends. May or may not chase their tail for no reason."
            emoji="🐕"></BigCard>
        </Link>
      </FlexView>
    </FlexView>
  );
}
