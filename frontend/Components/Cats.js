import { Container, Grid } from '@material-ui/core';
import axios from 'axios';
import React, { useEffect } from 'react';
import { SmallCard } from './SmallCard';

export function Cats() {
  const [cats, setCats] = React.useState([]);

  useEffect(() => {
    getCats();
  }, []);
  async function getCats() {
    try {
      const result = await axios.get('http://localhost:3000/cats');
      console.log(result.data);
      setCats(result.data);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <Container>
      <Grid container spacing={1}>
        {cats.map((cat) => (
          <Grid item>
            <SmallCard
              key={cat.name}
              propBirthday={cat.birthday}
              propDateAdopted={cat.dateAdopted}
              propName={cat.name}
              propType={cat.type}
              propAdoptedBy={cat.adoptedBy}
              propDescription={cat.description}
              imageUrl={cat.imageUrl}
              propStatus={cat.status}></SmallCard>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
