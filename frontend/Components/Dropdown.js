import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import React from 'react';

export function Dropdown({ label, inputLabel, menuItems }) {
  // sets default dropdown value to NYC
  const [value, setValue] = React.useState(menuItems[0].value);

  const handleChange = (event) => {
    setValue(event.target.value);
  };
  return (
    <FormControl
      style={{ backgroundColor: 'white', borderRadius: '5px' }}
      fullWidth={true}
      className="dropdown"
      sx={{ m: 1, minWidth: 80 }}>
      <InputLabel
        margin="dense"
        style={{ paddingLeft: '15px', fontWeight: 'bold' }}
        id="dropdown-label">
        <LocationOnIcon style={{ fontSize: '14px' }} /> {inputLabel}
      </InputLabel>
      <Select
        style={{ paddingLeft: '15px' }}
        labelId="demo-simple-select-standard-label"
        id="demo-simple-select-standard"
        value={value}
        onChange={handleChange}
        label={label}>
        {menuItems.map((item) => (
          <MenuItem key={item.value} value={item.value}>
            {item.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
