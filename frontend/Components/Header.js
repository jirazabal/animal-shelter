import { Breadcrumbs, styled } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import HomeIcon from '@material-ui/icons/Home';
import React from 'react';
import FlexView from 'react-flexview/lib';
import { Link } from 'react-router-dom';
import { Dropdown } from './Dropdown';
import './Header.scss';
export function Header() {
  // can get this from an API in the future which will contain location specific data
  const locations = [
    {
      value: 'NYC',
      name: 'New York City',
    },
  ];
  return (
    <AppBar position="static" style={{ backgroundColor: '#338f52' }}>
      <Toolbar variant="dense">
        <FlexView height="50%" marginTop="auto" basis={200}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}>
            <HomeIcon />
            <Link
              to={`/`}
              style={{
                paddingLeft: '15px',
                textDecoration: 'none',
                color: 'inherit',
              }}>
              Home
            </Link>
          </IconButton>
        </FlexView>
        <FlexView>
          <Breadcrumbs aria-label="breadcrumb">
            <Link
              style={{ color: 'white', fontSize: '18px' }}
              color="inherit"
              to="/cats">
              Cats
            </Link>
            <Link
              style={{ color: 'white', fontSize: '18px' }}
              color="textPrimary"
              to="/dogs"
              aria-current="page">
              Dogs
            </Link>
          </Breadcrumbs>
        </FlexView>
        <FlexView height="50%" marginLeft="auto" basis={200}>
          <Dropdown
            label="location"
            inputLabel="Location"
            menuItems={locations}></Dropdown>
        </FlexView>
      </Toolbar>
    </AppBar>
  );
}
