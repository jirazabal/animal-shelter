import { Container, Grid } from '@material-ui/core';
import axios from 'axios';
import React, { useEffect } from 'react';
import { SmallCard } from './SmallCard';

export function Dogs() {
  const [dogs, setDogs] = React.useState([]);

  useEffect(() => {
    getDogs();
  }, []);
  async function getDogs() {
    try {
      const result = await axios.get('http://localhost:3000/dogs');
      console.log(result.data);
      setDogs(result.data);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <Container>
      <Grid container spacing={1}>
        {dogs.map((dog) => (
          <Grid item>
            <SmallCard
              key={dog.name}
              propBirthday={dog.birthday}
              propDateAdopted={dog.dateAdopted}
              propName={dog.name}
              propType={dog.type}
              propAdoptedBy={dog.adoptedBy}
              propDescription={dog.description}
              imageUrl={dog.imageUrl}
              propStatus={dog.status}></SmallCard>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
