import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';
import { Cats } from './Cats';
import { Dogs } from './Dogs';
import { Home } from './Home';
import { Header } from './Header';

export function App() {
  // TODO: this can be done better
  //   fetch('http://localhost:3000/cats')
  //     .then((response) => response.json())
  //     .then((data) => setCats(data));

  // TODO: this can also be rendered differently
  // const renderCats = () =>
  //   cats.map((cat) => (
  //     <ul>
  //       <li>{cat.name}</li>
  //       <li>{cat.type}</li>
  //     </ul>
  //   ));

  return (
    <Router>
      <Header />

      <Switch>
        <Route path="/cats">
          <Cats />
        </Route>
        <Route path="/dogs">
          <Dogs />
        </Route>
        <Route path="/" exact>
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}
