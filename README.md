# Full Stack Application Exercise

**_Hi! We are super excited to see what you are able to create_**

**_Duration_**: We don't expect you to take more than 2~4h to finish the exercise.

## The Exercise

You are in the forefront of creating a new internal dashboard for an amazing Shelter.

This particular shelter only allows the intake of 2 types of animals **_Cats_** and **_Dogs_** both of which can always be adopted
at any stage.

As an Animal Care specialist I would love to be able to manage different aspects in this application:

- Identify the animal in an easy and intuitive way
- Understand how long the animal has been in the shelter
- Understand who has adopted our animals
- Be able to change information about the Cat or Dog easily.

Finally, you overheard the Shelter Manager mentioning that soon there is going to be a new shelter in a new location.
Not sure what this means now, but probably good to know.

**_What are we looking for_**

The following criteria will be used to evaluate your solution:

- ER diagram
- The application starts and doesn't have any visible errors
- Showcase your skills to us!

**_Important to know_**

We are not expecting you to create any database or any sql code, though we would like to see how you are thinking about the
database structure. It can also be a simple image or a readable hand drawing. All the data of the endpoints (Graphql or REST)
can be hardcoded as a JSON object return.

## System requirements

Make sure these application are installed in you machine. Those are necessary for the application to work

- [Nodejs]() - We are using v14.17.1
- [NPM]() - Latest version with the current system

## Starting the environment

First you need to install all the dependencies

```shell
npm i
```

Then you should be able to run the application with

```shell
npm start
```

## Notes

> There is currently no DB, so the JSON goes to the server but does not do anything. In a real life example, this would hit a database and upsert data for the user.

> Also, just a note, I have little to no experience writing React apps. I have more experience with Angular. I had to learn mostly everything coded in here for the sole purpose of this exercise and tried to instill any best practices I could find.
